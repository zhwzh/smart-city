package com.sc.businesslog.core.service;


import com.sc.businesslog.entity.BlOperationLog;
import com.sc.common.service.BaseService;


/**
 * Created by wust on 2019/5/28.
 */
public interface BlOperationLogService extends BaseService<BlOperationLog> {
}
