package com.sc.admin.core.api.feign;

import com.sc.admin.api.DistributedFileService;
import com.sc.admin.core.dao.SysDistributedFileMapper;
import com.sc.common.annotations.FeignApi;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@FeignApi
@RestController
public class DistributedFileFeignApi implements DistributedFileService {
    @Autowired
    private SysDistributedFileMapper sysDistributedFileMapper;


    @RequestMapping(value = INSERT,method= RequestMethod.POST)
    @Override
    public WebResponseDto insert(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysDistributedFile entity) {
        WebResponseDto responseDto = new WebResponseDto();
        sysDistributedFileMapper.insert(entity);
        responseDto.setObj(entity);
        return responseDto;
    }

    @RequestMapping(value = DELETE_BY_PRIMARY_KEY,method= RequestMethod.POST)
    @Override
    public WebResponseDto deleteByPrimaryKey(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestParam Long id) {
        WebResponseDto responseDto = new WebResponseDto();
        sysDistributedFileMapper.deleteByPrimaryKey(id);
        return responseDto;
    }


    @RequestMapping(value = SELECT_ONE,method= RequestMethod.POST)
    @Override
    public WebResponseDto selectOne(@RequestHeader(name = "x-ctx", required = true) String ctx, @RequestBody SysDistributedFile search) {
        WebResponseDto responseDto = new WebResponseDto();
        SysDistributedFile one = sysDistributedFileMapper.selectOne(search);
        responseDto.setObj(one);
        return responseDto;
    }
}
