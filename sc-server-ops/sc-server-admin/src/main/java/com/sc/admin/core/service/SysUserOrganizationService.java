package com.sc.admin.core.service;



import com.sc.common.entity.admin.userorganization.SysUserOrganization;
import com.sc.common.service.BaseService;

/**
 * Created by wust on 2019/8/9.
 */
public interface SysUserOrganizationService extends BaseService<SysUserOrganization> {
    /**
     * 初始化用户组织关系
     */
    void init();

    /**
     * 初始化指定用户的组织关系
     * @param userId
     */
    void initByUser(Long userId);
}
