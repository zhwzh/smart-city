package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.department.SysDepartment;

/**
 * Created by wust on 2019/6/3.
 */
public interface SysDepartmentMapper  extends IBaseMapper<SysDepartment> {
}
