package com.sc.admin.common.enums;


import com.sc.common.enums.MIMETypeEnum;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 需要缓存的表
 * Created by wust on 2019/3/29.
 */
public enum CacheTableNameEnum {

    SYS_ACCOUNT;

    public static Map<String, CacheTableNameEnum> valueLookup;

    static {
        valueLookup = new HashMap<>();
        for (CacheTableNameEnum cacheTableNameEnum : EnumSet.allOf(CacheTableNameEnum.class)) {
            valueLookup.put(cacheTableNameEnum.name(), cacheTableNameEnum);
        }
    }

    CacheTableNameEnum(){}

    public static CacheTableNameEnum findByValue(String value) {
        return valueLookup.get(value);
    }
}
