package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysDistributedFileMapper;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.exception.BusinessException;
import com.sc.common.service.MinioStorageService;
import com.sc.common.util.MyIdUtil;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.io.InputStream;
import java.util.Date;

@Service("minioStorageServiceImpl")
public class MinioStorageServiceImpl implements MinioStorageService {
    @Autowired
    private SysDistributedFileMapper sysDistributedFileMapper;

    @Autowired
    private MinioClient minioClient;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public SysDistributedFile upload(String bucketName, String objectName, InputStream stream, Long size, String contentType, String fileName, String fileType, String source) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        SysDistributedFile distributedFile = new SysDistributedFile();
        distributedFile.setId(MyIdUtil.getId());
        distributedFile.setBucketName(bucketName);
        distributedFile.setObjectName(objectName);
        distributedFile.setSize(size);
        distributedFile.setFileName(fileName);
        distributedFile.setFileType(fileType);
        distributedFile.setSource(source);
        distributedFile.setIsDeleted(0);
        distributedFile.setCreaterId(ctx.getAccountId());
        distributedFile.setCreaterName(ctx.getAccountName());
        distributedFile.setCreateTime(new Date());
        sysDistributedFileMapper.insert(distributedFile);


        try {
            minioClient.putObject(PutObjectArgs.builder().bucket(bucketName).object(objectName).stream(stream, size, -1).contentType(contentType).build());
        } catch (Exception e) {
            throw new BusinessException("上传文件出错：" + e.getMessage());
        }
        return distributedFile;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void delete(String bucketName, String objectName) {
        SysDistributedFile distributedFileSearch = new SysDistributedFile();
        distributedFileSearch.setBucketName(bucketName);
        distributedFileSearch.setObjectName(objectName);
        SysDistributedFile distributedFile = sysDistributedFileMapper.selectOne(distributedFileSearch);
        if(distributedFile != null){
            sysDistributedFileMapper.deleteByPrimaryKey(distributedFile.getId());
        }

        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
        } catch (Exception e) {
            throw new BusinessException("删除文件出错：" + e.getMessage());
        }
    }

    @Override
    public void deleteDistributedFileByPrimaryKey(Long fileId) {
        SysDistributedFile distributedFile = sysDistributedFileMapper.selectByPrimaryKey(fileId);
        if(distributedFile != null){
            sysDistributedFileMapper.deleteByPrimaryKey(distributedFile.getId());
        }

        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(distributedFile.getBucketName()).object(distributedFile.getObjectName()).build());
        } catch (Exception e) {
            throw new BusinessException("删除文件出错：" + e.getMessage());
        }
    }

    @Override
    public SysDistributedFile selectDistributedFileByPrimaryKey(Long fileId) {
        return sysDistributedFileMapper.selectByPrimaryKey(fileId);
    }
}
