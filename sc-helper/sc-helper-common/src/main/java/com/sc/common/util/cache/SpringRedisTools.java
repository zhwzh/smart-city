package com.sc.common.util.cache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis操作工具
 */
@Service("springRedisTools")
public final class SpringRedisTools {

    static Logger logger = LogManager.getLogger(SpringRedisTools.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public Set<String> keys(String key){
        return redisTemplate.keys(key);
    }

    public boolean hasKey(String key){
        return redisTemplate.hasKey(key);
    }

    /**
     * 根据key获取缓存值
     *
     * @param key
     * @return
     */
    public Object getByKey(String key) {
        ValueOperations<String, Object> ops = redisTemplate.opsForValue();
        Object obj = ops.get(key);
        return obj;
    }

    /**
     * 根据key值删除缓存值
     *
     * @param key
     */
    public void deleteByKey(String key) {
        redisTemplate.delete(key);
        logger.warn("删除redis数据，key=[{}]",key);
    }

    /**
     * 根据key值删除缓存值
     *
     * @param keys
     */
    public void deleteByKey(Set<String> keys) {
        redisTemplate.delete(keys);
        logger.warn("批量删除redis数据，keys=[{}]",keys);
    }

    /**
     * 根据key值更新缓存
     *
     * @param key
     * @param value
     */
    public void updateDataByKey(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 往缓存中增加值，不会覆盖原来的key
     * Set {@code value} for {@code key}, only if {@code key} does not exist.
     *
     * @param key
     * @param value
     */
    public void addData(String key, Object value) {
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        opsForValue.setIfAbsent(key, value);
    }


    /**
     * 往缓存中增加值,带过期时间
     *
     * @param key
     * @param value
     * @param timeout
     */
    public void addData(String key, Object value, long timeout, TimeUnit unit) {
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        opsForValue.set(key, value);
        redisTemplate.expire(key, timeout, unit);
    }

    public void updateExpire(final String key, long expireTime, TimeUnit timeUnit) {
        redisTemplate.expire(key, expireTime, timeUnit);
        logger.warn("跟新redis数据，key=[{}], expireTime=[{}],timeUnit=[{}]",key,expireTime,timeUnit);
    }

    /**
     * 基于long类型的原子自增操作
     * @param key
     * @param value
     * @param timeout
     * @param unit
     * @return
     */
    public Long incrementForLong(String key, long value, long timeout, TimeUnit unit){
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        Long returnValue = opsForValue.increment(key,value);
        if(timeout > 0){
            redisTemplate.expire(key, timeout, unit);
        }
        return returnValue;
    }



    /**
     * 添加Map，会覆盖原来的值
     * @param key
     * @param value map对象
     */
    public void addMap(String key, Map value) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.putAll(key,value);
    }

    /**
     * 添加map，带过期参数
     * @param key
     * @param value
     * @param timeout
     * @param unit
     */
    public void addMap(String key, Map value, long timeout, TimeUnit unit) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.putAll(key,value);
        redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 往指定的map中添加一个字段
     * @param key
     * @param fieldName
     * @param value
     */
    public void addFieldToMap(String key, String fieldName,Object value) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.put(key,fieldName,value);
    }

    /**
     * 往指定的map中添加一个字段，带过期时间
     * @param key
     * @param fieldName
     * @param value
     */
    public void addFieldToMap(String key, String fieldName,Object value, long timeout, TimeUnit unit) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.put(key,fieldName,value);
        redisTemplate.expire(key, timeout, unit);
    }

    /**
     * 根据key获取Map
     * @param key
     * @return
     */
    public Map getMap(String key) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        return opsForHash.entries(key);
    }

    /**
     * 根据fieldName获取value
     * @param key
     * @param fieldName
     * @return
     */
    public Object getMapValueByFieldName(String key,Object fieldName) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        return opsForHash.get(key,fieldName);
    }

    /**
     * 删除map
     * @param key
     */
    public void deleteMap(String key) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.getOperations().delete(key);
    }

    /**
     * 从map里面删除指定的字段
     * @param key
     * @param fieldNames
     */
    public void deleteFieldFromMap(String key,Object... fieldNames) {
        HashOperations<String, Object,Object> opsForHash = redisTemplate.opsForHash();
        opsForHash.delete(key,fieldNames);
    }
}
