package com.sc.common.entity.admin.blacklist;

import com.sc.common.entity.BaseEntity;

import javax.persistence.Table;

@Table(name = "sys_blacklist")
public class SysBlacklist extends BaseEntity {
    private String ip;

    private String uri;

    private String user;

    private String description;


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}