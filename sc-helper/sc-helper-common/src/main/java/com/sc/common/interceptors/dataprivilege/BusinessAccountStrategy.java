package com.sc.common.interceptors.dataprivilege;

import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.util.ReflectHelper;
import com.sc.common.util.cache.SpringRedisTools;
import org.apache.ibatis.executor.statement.BaseStatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:12
 * @description：操作层用户策略，默认能看到自己公司或自己创建的数据，后期可以增加其他限制，比如只能看自己、部门、岗位等数据
 * @version:
 */
@Service("businessAccountStrategy")
public class BusinessAccountStrategy implements IStrategy {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Override
    public void bindSql(BaseStatementHandler delegate) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        BoundSql boundSql = delegate.getBoundSql();

        String sql = boundSql.getSql();

        StringBuffer privilegeSqlStringBuffer = new StringBuffer("SELECT privilege_tmp.* FROM (" + sql + ") privilege_tmp");
        privilegeSqlStringBuffer.append(" WHERE company_id = " + ctx.getBranchCompanyId());
        privilegeSqlStringBuffer.append(" OR creater_id = " + ctx.getAccountId());
        ReflectHelper.setValueByFieldName(boundSql, "sql", privilegeSqlStringBuffer.toString());
    }
}
