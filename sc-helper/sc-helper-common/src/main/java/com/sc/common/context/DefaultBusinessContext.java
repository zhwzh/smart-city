package com.sc.common.context;

import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.util.CommonUtil;

/**
 * Created by wust on 2019/5/6.
 */
public class DefaultBusinessContext extends BaseBusinessContext{

    private static final long serialVersionUID = -3524432430765687155L;
    private DefaultBusinessContext(){}

    private static final ThreadLocal<DefaultBusinessContext> LOCAL = new ThreadLocal<DefaultBusinessContext>() {
        protected DefaultBusinessContext initialValue() {
            return new DefaultBusinessContext();
        }
    };

    public static DefaultBusinessContext getContext() {
        return LOCAL.get();
    }


    public static void copyToCurrentThread(DefaultBusinessContext ctx){
        DefaultBusinessContext.getContext().setDataSourceId(ctx.getDataSourceId());
        DefaultBusinessContext.getContext().setLocale(ctx.getLocale());
        DefaultBusinessContext.getContext().setBranchCompanyId(ctx.getBranchCompanyId());
        DefaultBusinessContext.getContext().setParentCompanyId(ctx.getParentCompanyId());
        DefaultBusinessContext.getContext().setCustomer(ctx.getCustomer());
        DefaultBusinessContext.getContext().setAccountType(ctx.getAccountType());
        DefaultBusinessContext.getContext().setAccountName(ctx.getAccountName());
        DefaultBusinessContext.getContext().setAccountCode(ctx.getAccountCode());
        DefaultBusinessContext.getContext().setAccountId(ctx.getAccountId());
        DefaultBusinessContext.getContext().setAgentId(ctx.getAgentId());
        DefaultBusinessContext.getContext().setUser(ctx.getUser());
        DefaultBusinessContext.getContext().setRedisKey(ctx.getRedisKey());
        DefaultBusinessContext.getContext().setxAuthToken(ctx.getxAuthToken());
        DefaultBusinessContext.getContext().setProjectId(ctx.getProjectId());
    }

    private Long accountId;
    private String accountCode;
    private String accountName;
    private String accountType;
    private SysUser user;             // 员工
    private SysCustomer customer;     // 客户
    private String xAuthToken;
    private String redisKey;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public SysCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(SysCustomer customer) {
        this.customer = customer;
    }

    public String getxAuthToken() {
        return xAuthToken;
    }

    public void setxAuthToken(String xAuthToken) {
        this.xAuthToken = xAuthToken;
    }

    public String getRedisKey() {
        return redisKey;
    }

    public void setRedisKey(String redisKey) {
        this.redisKey = redisKey;
    }

    public boolean isSuperAdmin(){
        if(CommonUtil.isSuperAdminAccount(this.getAccountType())) {
            return true;
        }
        return false;
    }

    public boolean isAdmin(){
        if(CommonUtil.isAdminAccount(this.getAccountType())) {
            return true;
        }
        return false;
    }

    public boolean isStaff(){
        if(CommonUtil.isStaffAccount(this.getAccountType())) {
            return true;
        }
        return false;
    }

    /**
     * 获取userType，主要用于获取菜单、组织参数
     * @return
     */
    public String getPermissionType(){
        String permissionType = "";
        if(isSuperAdmin() || isAdmin()){ // 系统超级管理员
            permissionType = "A100401";
        }else if(isStaff()){ // 员工
            permissionType = this.getUser().getType();
        }
        return permissionType;
    }
}
