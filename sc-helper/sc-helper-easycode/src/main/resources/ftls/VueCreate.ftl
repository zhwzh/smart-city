/**
* Created by ${Author} on ${DateTime}.
*/
<template>
  <el-form ref="formModel" :model="formModel" :rules="rules" label-width="120px" style="width:70%;" @submit.native.prevent>
    ${formItem}
    <el-form-item style="text-align: left;">
      <el-button type="primary" @click="onSubmit('formModel')" :loading="submitting">提交</el-button>
    </el-form-item>
  </el-form>
</template>
<script>
import Vue from 'vue'

export default {
  name: '${name}-create',
  data () {
    return {
      submitting: false,
      formModel: {
        ${formModelProperty}
      },
      rules: {
      }
    }
  },
  methods: {
    onSubmit: function (formData) {
      this.$refs[formData].validate((valid) => {
        if (!valid) {
        } else {
          this.submitting = true
          Vue.$ajax({
            method: 'post',
            url: ${axiosReqPrefix} + '/web/v1/${moduleName}Controller',
            data: this.formModel
          }).then(res => {
            this.submitting = false
            if (res.data.flag !== 'SUCCESS') {
              if (!Vue.$isNullOrIsBlankOrIsUndefined(res.data.message)) {
                this.$message({
                  message: res.data.message,
                  type: 'warning'
                })
              }
            } else {
              this.$message({
                message: res.data.message,
                type: 'success'
              })
              this.resetForm('formModel')
            }
          })
        }
      })
    },
    resetForm (formData) {
      this.$refs[formData].resetFields()
    }
  }
}
</script>
