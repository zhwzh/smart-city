package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class VueSearchBarTask extends AbstractTask {
    public VueSearchBarTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String postfixName =  super.getName()[0];
        String name = super.getName()[1];

        String fileName = name + "-search-bar.vue";

        Map<String, String> daoData = new HashMap<>();
        daoData.put("Author", ConfigUtil.getConfiguration().getAuthor());
        daoData.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        daoData.put("moduleName", postfixName);
        daoData.put("name", name);
        daoData.put("pageName", postfixName + "List");
        daoData.put("axiosReqPrefix", ConfigUtil.getConfiguration().getAxiosReqPrefix());

        String filePath = StringUtil.package2Path(ConfigUtil.getConfiguration().getPath().getVueDir());
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_VUE_SEARCH_BAR, daoData, filePath + File.separator + name,fileName);
    }
}
