package com.sc.generatorcode.utils;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.*;
import com.sc.generatorcode.task.*;
import com.sc.generatorcode.task.base.AbstractTask;

import java.util.LinkedList;
import java.util.List;

/**
 * @author ：wust
 * Date   2018-11-27
 */
public class TaskQueue {

    private LinkedList<AbstractTask> taskQueue = new LinkedList<>();

    private void initCommonTasks(String className, List<ColumnInfo> tableInfos) {
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getBasePackage().getController())) {
            taskQueue.add(new ControllerTask(className));
        }
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getBasePackage().getService())) {
            taskQueue.add(new ServiceImplTask(className));
        }
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getBasePackage().getInterf())) {
            taskQueue.add(new ServiceTask(className));
        }
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getBasePackage().getDao())) {
            taskQueue.add(new MapperTask(className));
        }
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getPath().getVueDir())) {
            taskQueue.add(new VueListTask(className,tableInfos));
            taskQueue.add(new VueCreateTask(className,tableInfos));
            taskQueue.add(new VueUpdateTask(className,tableInfos));
            taskQueue.add(new VueSearchBarTask(className,tableInfos));
            taskQueue.add(new VueImportTask(className,tableInfos));
            taskQueue.add(new VueRouteTask(className,tableInfos));
            taskQueue.add(new MenuTask(className,tableInfos));
            taskQueue.add(new ImportExcelXmlTask(className,tableInfos));
            taskQueue.add(new ImportServiceTask(className));
            taskQueue.add(new ImportServiceImplTask(className));
        }
    }

    public void initSingleTasks(String className, String tableName, List<ColumnInfo> tableInfos) {
        initCommonTasks(className,tableInfos);
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getBasePackage().getEntityPackageName())) {
            taskQueue.add(new EntityTask(className, tableInfos));
        }
        if (!StringUtil.isBlank(ConfigUtil.getConfiguration().getPath().getMapper())) {
            taskQueue.add(new MapperXmlTask(className, tableName, tableInfos));
        }

        taskQueue.add(new ExportExcelXmlTask(className,tableName,tableInfos));
    }


    public boolean isEmpty() {
        return taskQueue.isEmpty();
    }

    public AbstractTask poll() {
        return taskQueue.poll();
    }

}
