package com.sc.admin.api.fallback;

import com.sc.admin.api.DistributedFileService;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import org.springframework.stereotype.Component;

/**
 * 服务降级配置
 */
@Component
public class DistributedFileServiceFallback implements DistributedFileService {
    @Override
    public WebResponseDto insert(String ctx, SysDistributedFile entity) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }

    @Override
    public WebResponseDto deleteByPrimaryKey(String ctx, Long id) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }

    @Override
    public WebResponseDto selectOne(String ctx, SysDistributedFile search) {
        WebResponseDto webResponseDto = new WebResponseDto();
        webResponseDto.setFlag(WebResponseDto.INFO_ERROR);
        webResponseDto.setMessage("服务异常");
        return webResponseDto;
    }
}
