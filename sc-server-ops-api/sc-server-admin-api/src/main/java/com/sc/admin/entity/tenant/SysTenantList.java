package com.sc.admin.entity.tenant;

import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
  @ApiModel(description = "列表对象-SysTenantList")
  @NoArgsConstructor
  @ToString
  @Data
public class SysTenantList extends SysTenant {

}