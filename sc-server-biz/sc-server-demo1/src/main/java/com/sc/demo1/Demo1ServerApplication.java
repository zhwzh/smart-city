/**
 * Created by wust on 2019-11-11 14:00:17
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.demo1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author: wust
 * @date: Created in 2019-11-11 14:00:17
 * @description:
 *
 */
@EnableSwagger2
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.demo1.core.dao"})
@SpringBootApplication(scanBasePackages = {"com.sc"})
public class Demo1ServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(Demo1ServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(Demo1ServerApplication.class, args);
    }
}
