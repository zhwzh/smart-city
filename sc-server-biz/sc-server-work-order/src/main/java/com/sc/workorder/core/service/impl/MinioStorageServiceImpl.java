package com.sc.workorder.core.service.impl;


import com.alibaba.fastjson.JSONObject;
import com.sc.admin.api.DistributedFileService;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.exception.BusinessException;
import com.sc.common.service.MinioStorageService;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.ServerSideEncryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

@Service("minioStorageServiceImpl")
public class MinioStorageServiceImpl implements MinioStorageService {
    @Autowired
 private DistributedFileService distributedFileService;

    @Autowired
    private MinioClient minioClient;


    @Override
    public SysDistributedFile upload(String bucketName, String objectName, InputStream stream, Long size, String contentType, String fileName, String fileType, String source) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        try {
            minioClient.putObject(PutObjectArgs.builder().bucket(bucketName).object(objectName).stream(stream, size, -1).contentType(contentType).build());
        } catch (Exception e) {
            throw new BusinessException("上传文件出错：" + e.getMessage());
        }

        SysDistributedFile distributedFile = new SysDistributedFile();
        distributedFile.setBucketName(bucketName);
        distributedFile.setObjectName(objectName);
        distributedFile.setSize(size);
        distributedFile.setFileName(fileName);
        distributedFile.setFileType(fileType);
        distributedFile.setSource(source);
        distributedFile.setCreaterId(ctx.getAccountId());
        distributedFile.setCreaterName(ctx.getAccountName());
        distributedFile.setCreateTime(new Date());
        WebResponseDto responseDto = distributedFileService.insert(JSONObject.toJSONString(ctx),distributedFile);
        if(!responseDto.isSuccess()){
            try {
                minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
            } catch (Exception e) {
            }
            throw new BusinessException(responseDto.getMessage());
        }

        SysDistributedFile distributedFileWithId = JSONObject.parseObject(JSONObject.toJSONString(responseDto.getObj()),SysDistributedFile.class);
        return distributedFileWithId;
    }


    @Override
    public void delete(String bucketName, String objectName) {
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysDistributedFile distributedFileSearch = new SysDistributedFile();
        distributedFileSearch.setBucketName(bucketName);
        distributedFileSearch.setObjectName(objectName);
        WebResponseDto responseDto = distributedFileService.selectOne(JSONObject.toJSONString(ctx),distributedFileSearch);
        if(!responseDto.isSuccess()){
            throw new BusinessException(responseDto.getMessage());
        }

        SysDistributedFile distributedFile = JSONObject.parseObject(JSONObject.toJSONString(responseDto.getObj()),SysDistributedFile.class);
        if(distributedFile != null){
            distributedFileService.deleteByPrimaryKey(JSONObject.toJSONString(ctx),distributedFile.getId());
        }

        try {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
        } catch (Exception e) {
            throw new BusinessException("删除文件出错：" + e.getMessage());
        }
    }

    @Override
    public void deleteDistributedFileByPrimaryKey(Long fileId) {

    }

    @Override
    public SysDistributedFile selectDistributedFileByPrimaryKey(Long fileId) {
        return null;
    }
}
